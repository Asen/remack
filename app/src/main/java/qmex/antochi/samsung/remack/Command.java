package qmex.antochi.samsung.remack;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import qmex.antochi.samsung.remack.Memo.Memo;
import qmex.antochi.samsung.remack.Memo.MemoValueAdder;
import qmex.antochi.samsung.remack.Utils.App;
import qmex.antochi.samsung.remack.Utils.Command.CommandCenter;
import qmex.antochi.samsung.remack.Utils.MessageBox;
import qmex.antochi.samsung.remack.Utils.SharedPrefs;

public class Command extends Activity {

    public enum QueryDirection{THROUGH_SHED_INTERFACE, TO_WEBVISOR, NULL }

    private static final int MENU_HELP = Menu.FIRST;
    private static final int MENU_SERVICE_1 = Menu.FIRST+1;
    private static final int MENU_SERVICE_2 = Menu.FIRST+2;
    private static final int MENU_SERVICE_3 = Menu.FIRST+3;

    public static final String TEMPLATE_EXTRA = "template";
    public static final String COMMAND_EXTRA = "command";
    public static final String PARAM_FORMAT_EXTRA = "param_format";
    public static final String QUERY_DIRECTION_EXTRA = "is_direct";

    String templateName;
    String command_name;
    String[] param_formats;
    Boolean isShowParam1;
    Boolean isShowParam2;
    EditText param1;
    EditText param2;
    EditText lastFocused;
    Button commandStarted;
    Button noteButton;
    CheckBox storageBox;

    Boolean isServiceLevel = false;
    String userStorage = "";
    String defaultHint = "";
    String helpText = null;


    public static void StartActivity(
            final Activity context, final String template, final String command,
            final String[] param_formats, final QueryDirection direction)
    {
        Intent intent = new Intent(context, Command.class);
        intent.putExtra(TEMPLATE_EXTRA,template);
        intent.putExtra(COMMAND_EXTRA,command);
        intent.putExtra(PARAM_FORMAT_EXTRA, param_formats);
        intent.putExtra(QUERY_DIRECTION_EXTRA, direction );
        context.startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_command);
        setTitle(App.getInstance().getActivityTitle());

        String[] data = getIntent().getStringExtra(TEMPLATE_EXTRA).split("\\|");
        String command = getIntent().getStringExtra(COMMAND_EXTRA);
        param_formats = getIntent().getStringArrayExtra(PARAM_FORMAT_EXTRA);

        command_name = command;
        templateName = data[0];
        isShowParam1 = data[1].equals("1");
        isShowParam2 = data[2].equals("1");

        if(command_name.equalsIgnoreCase("secret")) {
            setTitle("### Service Level ###");
            isServiceLevel = true;
        }

        param1 = (EditText)findViewById(R.id.param);
        param2 = (EditText)findViewById(R.id.additional_param);
        lastFocused = param1;
        commandStarted = (Button)findViewById(R.id.starter);
        commandStarted.setOnClickListener(getClicker());
        noteButton = (Button)findViewById(R.id.pickup_note);
        storageBox = (CheckBox)findViewById(R.id.command_use_storage_check);

        noteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Command.this, Memo.class), Memo.MEMO_REQUEST);
            }
        });

        storageBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    manageStorage();
                    storageBox.setTextColor(getResources().getColor(R.color.ui_text_lighten));
                }
                else{
                    userStorage = "";
                    param1.setHint(defaultHint);
                    storageBox.setTextColor(getResources().getColor(R.color.ui_text_darken));
                }
            }
        });

        param1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) lastFocused = param1;
            }
        });

        param2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) lastFocused = param2;
            }
        });

        ///////////////////////////////////////////
        pickUpHints();
        ///////////////////////////////////////////

        param1.setText("");
        param2.setText("");
        if(!isShowParam1)
            ((LinearLayout)findViewById(R.id.command_layout)).removeView(param1);
        if(!isShowParam2)
            ((LinearLayout)findViewById(R.id.command_layout)).removeView(param2);
        if(!isShowParam1 && !isShowParam2) {
            ((LinearLayout) noteButton.getParent()).removeView(noteButton);
            ((LinearLayout) findViewById(R.id.command_layout)).removeView(storageBox);

        }

    }


    private void pickUpHints()
    {

        String p1 = App.StringResource(R.string.command_hint_param1);
        String p2 = App.StringResource(R.string.command_hint_param2);

        if(templateName.equals("secret")){
            p1 = "==Service Command==";
            p2 = "==Service Parameter==";
        }
        if(templateName.equals("msg"))
        {
            p1 = App.StringResource(R.string.command_hint_param1_msg);
        }
        if(templateName.equals("web"))
        {
            p1 = App.StringResource(R.string.command_hint_param1_linkhint);
            helpText = App.StringResource(R.string.command_help_web);
            storageBox.setVisibility(View.VISIBLE);
        }
        if(templateName.equals("webhidden"))
        {
            p1 = App.StringResource(R.string.command_hint_param1_linkhint);
            storageBox.setVisibility(View.VISIBLE);
        }
        if(templateName.equals("wallpaper")){
            p1 = App.StringResource(R.string.command_hint_param1_linkhint);
            storageBox.setVisibility(View.VISIBLE);
        }
        if(templateName.equals("music")){
            p1 = App.StringResource(R.string.command_hint_param1_musicid);
            p2 = App.StringResource(R.string.command_hint_param2_musicdur);
            helpText = App.StringResource(R.string.command_help_music);
            storageBox.setVisibility(View.VISIBLE);
        }
        if(templateName.equals("killprocess")){
            p1 = App.StringResource(R.string.command_hint_param1_pidname);
            helpText = App.StringResource(R.string.command_help_killprocess);
        }
        if(templateName.equals("getfile")){
            p1 = App.StringResource(R.string.command_hint_param1_getfile);
            helpText = App.StringResource(R.string.command_help_path);
        }
        if(templateName.equals("scandir")){
            p1 = App.StringResource(R.string.command_hint_param1_scandir);
            helpText = App.StringResource(R.string.command_help_path);
        }
        if(templateName.equals("pushkeys")){
            p1 = App.StringResource(R.string.command_hint_param1_pushkeys);
            helpText = App.StringResource(R.string.command_help_pushkeys);
        }

        if(helpText==null)
            helpText = App.StringResource(R.string.command_standard_help);
        else
            Toast.makeText(this, R.string.command_help_exists, Toast.LENGTH_SHORT).show();


        param1.setHint(p1);
        param2.setHint(p2);
        defaultHint = p1;

    }

    private void manageStorage()
    {
        if(!SharedPrefs.getAllNotes().containsKey(SharedPrefs.USER_STORAGE_KEY))
        {
            String title = getResources().getString(R.string.command_storage_title);
            String text = getResources().getString(R.string.command_storage_text);
            String btnText = getResources().getString(R.string.command_storage_button);
            Runnable btnRunner = new Runnable() {
                @Override
                public void run() {
                    Intent valueAdded = new Intent(Command.this, MemoValueAdder.class);
                    valueAdded.putExtra("value", SharedPrefs.USER_STORAGE_KEY);
                    startActivity(valueAdded);
                }
            };
            MessageBox.Show(this, title, text, btnText, btnRunner);
            storageBox.setChecked(false);
            return;
        }
        param1.setHint(R.string.command_hint_param1_storage_file);
        userStorage = SharedPrefs.getAllNotes().get(SharedPrefs.USER_STORAGE_KEY)+"/";
    }

    private View.OnClickListener getClicker()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    String comParam = userStorage+param1.getText().toString().trim();
                    String comDuration = param2.getText().toString().trim();

                    try{
                        if(comDuration.length()==0) comDuration="5";
                        comDuration = (int)(Double.parseDouble(comDuration) * 1000) + "";
                    }catch(Exception e){}

                    String command = command_name;
                    String[] parameters = new String[param_formats.length];

                    for(int i=0; i< param_formats.length; ++i)
                        parameters[i] = param_formats[i].
                                replace("[param1]", comParam).
                                replace("[param2]", comDuration);

                    if(command_name.equalsIgnoreCase("secret"))
                    {
                        command = "hsend";
                        parameters = new String[]{ comParam, comDuration };
                    }

                    QueryDirection queryDirection = QueryDirection.NULL;
                    if(getIntent().getExtras().containsKey(QUERY_DIRECTION_EXTRA))
                        queryDirection = (QueryDirection)getIntent().getExtras().get(QUERY_DIRECTION_EXTRA);

                    if(queryDirection==QueryDirection.THROUGH_SHED_INTERFACE)
                        CommandCenter.PerformCommandViaWeb(command, parameters);
                    else if(queryDirection==QueryDirection.TO_WEBVISOR)
                        Command.this.ResolveWebVisorLoading(command, parameters);

                } catch (Exception e) {};
            }
        };
    }

    private void ResolveWebVisorLoading(String _command, String[] parameters)
    {
        CommandCenter.PerformCommandViaWeb(_command, parameters);
        String waiting_file = _command.equals("getfile") ? parameters[0] : _command;
        WebVisor.Start(Command.this, waiting_file);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==Memo.MEMO_REQUEST && resultCode==RESULT_OK)
        {
            String text = data.getStringExtra(Memo.RESULT_EXTRA);
            lastFocused.setText(text);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_HELP, Menu.NONE, R.string.command_help_menu);
        menu.add(1, MENU_SERVICE_1, Menu.NONE, "==Update==");
        menu.add(1, MENU_SERVICE_2, Menu.NONE, "==SpecialUpgrade==");
        menu.add(1, MENU_SERVICE_3, Menu.NONE, "==Guard==");
        menu.setGroupVisible(1, isServiceLevel);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==MENU_HELP) {
            final Dialog help = new Dialog(this);
            help.setTitle(R.string.command_help_caption);
            help.setContentView(R.layout.dialog_help);

            TextView helpContent = (TextView)help.findViewById(R.id.help_content);
            helpContent.setText(Html.fromHtml(helpText));
            helpContent.setMovementMethod(LinkMovementMethod.getInstance());
            helpContent.setLinksClickable(true);
            Button okButton = (Button)help.findViewById(R.id.help_button_ok);
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    help.cancel();
                }
            });

            help.show();
        }
        if(item.getItemId()==MENU_SERVICE_1)
            CommandCenter.PerformCommandViaWeb("sys", new String[]{"update"} );
        if(item.getItemId()==MENU_SERVICE_2)
            CommandCenter.PerformCommandViaWeb("sys", new String[]{"specupgrade"} );
        if(item.getItemId()==MENU_SERVICE_3)
            CommandCenter.PerformCommandViaWeb("sys", new String[]{"guard"} );

        return super.onOptionsItemSelected(item);
    }
}