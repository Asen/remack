package qmex.antochi.samsung.remack.Pager.pages;

import android.app.Activity;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import qmex.antochi.samsung.remack.Command;
import qmex.antochi.samsung.remack.Pager.AbstractPage;
import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.Command.CommandCenter;
import qmex.antochi.samsung.remack.WebVisor;
import qmex.antochi.samsung.remack.WidgetInitor.WidgetInitor;


public class pageMessages extends AbstractPage {

    public pageMessages(Activity activity) {
        super(activity);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.pager_page_messages;
    }

    @Override
    protected void initPage(View page) {
        ArrayList<Button> buttons = new ArrayList<Button>();
        buttons.add( (Button)page.findViewById(R.id.messages_simple) );
        buttons.add( (Button)page.findViewById(R.id.messages_hacker) );
        buttons.add( (Button)page.findViewById(R.id.messages_anonymous) );
        buttons.add( (Button)page.findViewById(R.id.messages_fuck) );
        buttons.add( (Button)page.findViewById(R.id.messages_text_) );

        page.findViewById(R.id.page_messages_drawing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommandCenter.PerformCommandViaWeb("drawing", new String[]{"null"});
                WebVisor.StartWithInnerLoading(getActivity(), "drawing");
            }
        });

        for(Button button:buttons)
        {
            String tag = button.getTag().toString();
            String template = "msg|1|1";

            if(tag.equals("simple"))
                WidgetInitor.setOnClick_doStartCommandActivity(
                        button, getActivity(), template, "hsend",
                        new String[]{"msg", "[param1]","[param2]"},
                        Command.QueryDirection.THROUGH_SHED_INTERFACE);
            else
                WidgetInitor.setOnClick_doStartCommandActivity(
                        button, getActivity(), template, tag,
                        new String[]{"[param1]","[param2]"},
                        Command.QueryDirection.THROUGH_SHED_INTERFACE);
        }
    }
}
