package qmex.antochi.samsung.remack.Utils.Command;

import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.App;

public abstract class FileUploader {

    public static void Upload(final File file, final String specialParam)
    {
        Toast.makeText(App.getInstance().getApplicationContext(),
                R.string.uploader_please_wait, Toast.LENGTH_LONG).show();

        new AsyncTask<Void,Void,String>(){
            @Override
            protected String doInBackground(Void... params) {
                StringBuilder data = new StringBuilder();
                try {
                    FileInputStream fis = new FileInputStream(file);
                    byte[] bytes = new byte[16384];
                    while (fis.read(bytes) != -1)
                        data.append( new String(bytes,"ISO8859-1") );
                    fis.close();
                } catch (Exception e) { }

                //file.delete();
                if(data.length() == 0) return null;
                //Log.i("LOAGING", "Reading completed~");

                HttpClient httpclient = new DefaultHttpClient();
                String url = URLCompleter.CompleteWeb("sys", new String[]{"load", specialParam.trim()});
                HttpPost httppost = new HttpPost(url);
                ArrayList<NameValuePair> nameValuePairs = new  ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("data", data.toString()));
                try {
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpclient.execute(httppost);
                } catch (Exception e) {}


                return null;
            }

            @Override
            protected void onPostExecute(String aVoid) {
                //Log.i("LOAGING", "HOORAY");
                Toast.makeText(App.getInstance().getApplicationContext(),
                        R.string.uploader_uploading_completed, Toast.LENGTH_SHORT).show();
            }
        }.execute();
    }

}
