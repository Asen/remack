package qmex.antochi.samsung.remack.Pager.pages;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import qmex.antochi.samsung.remack.Memo.Memo;
import qmex.antochi.samsung.remack.Pager.AbstractPage;
import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.App;
import qmex.antochi.samsung.remack.Utils.Command.CommandCenter;
import qmex.antochi.samsung.remack.Utils.MessageBox;

public class pageModules extends AbstractPage {

    public static final int MEMO_STORAGE_REQUEST = 0x50;
    private final String BASE_MODULE_COLLECTION = "BASE_MODULE_COLLECTION";
    private String currentModuleCollection = BASE_MODULE_COLLECTION;

    TextView usingCollText = null;
    CheckBox useMyColl = null;
    EditText name = null;
    EditText param = null;
    Button schedule = null;

    public pageModules(Activity activity) {
        super(activity);
    }

    @Override
    public void processActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == pageModules.MEMO_STORAGE_REQUEST)
        {
            String key = resultCode == Activity.RESULT_OK ? data.getStringExtra(Memo.RESULT_KEY_EXTRA) : null;
            String value = resultCode == Activity.RESULT_OK ? data.getStringExtra(Memo.RESULT_EXTRA) : null;

            if(resultCode==Activity.RESULT_CANCELED || key==null || value==null){
                useMyColl.setChecked(false);
                return;
            }

            currentModuleCollection = value;

            String caption = App.StringResource(R.string.page_modules_mbox_caption);
            String btnText = App.StringResource(R.string.page_modules_mbox_btn);
            value = "<a href='"+value+"'>"+value+"</a>";
            MessageBox.ShowInfoDialog(getActivity(), caption, value, btnText, new Runnable() {
                @Override
                public void run() {
                    usingCollText.setText(R.string.page_modules_collection_custom);
                }
            });
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.pager_page_modules;
    }

    @Override
    protected void initPage(View page) {

        name = (EditText)page.findViewById(R.id.sheduller_name);
        param = (EditText)page.findViewById(R.id.sheduller_param);
        schedule = (Button)page.findViewById(R.id.schduller_schedule);
        usingCollText = (TextView)page.findViewById(R.id.modules_collection);
        useMyColl = (CheckBox)page.findViewById(R.id.modules_use_custom);

        useMyColl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                currentModuleCollection = BASE_MODULE_COLLECTION;
                usingCollText.setText(R.string.page_modules_collection_base);
                if(isChecked) tryChangeModuleStorage();
            }
        });

        schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processModule( name.getText().toString(), param.getText().toString());
            }
        });

        TextView howUse = (TextView)page.findViewById(R.id.schduller_manual);
        String text = App.StringResource(R.string.page_modules_how_use_link);
        howUse.setText(Html.fromHtml("<u>" + text + "</u>"));
        howUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uriAboutPage = Uri.parse(App.StringResource(R.string.app_modules_help_page));
                Intent aboutModulesPage = new Intent(Intent.ACTION_VIEW, uriAboutPage);
                getActivity().startActivity(aboutModulesPage);
            }
        });

    }

    private void processModule(String name, String param)
    {
        try {
            String concat = currentModuleCollection.trim().endsWith("/") ? "":"/";
            name = (currentModuleCollection)+(concat)+(name.trim().toLowerCase());
            param = param.trim();
            CommandCenter.PerformCommandViaWeb("pmodule", new String[]{ name, param });
        } catch (Exception e) {  }
    }

    public void tryChangeModuleStorage()
    {
        Intent storagePicker = new Intent(getActivity(), Memo.class);
        getActivity().startActivityForResult(storagePicker, MEMO_STORAGE_REQUEST);
    }

}
