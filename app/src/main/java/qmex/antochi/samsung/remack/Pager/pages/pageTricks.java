package qmex.antochi.samsung.remack.Pager.pages;

import android.app.Activity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

import qmex.antochi.samsung.remack.Command;
import qmex.antochi.samsung.remack.Pager.AbstractPage;
import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.Command.CommandCenter;
import qmex.antochi.samsung.remack.WidgetInitor.WidgetInitor;

public class pageTricks extends AbstractPage {

    public pageTricks(Activity activity) {
        super(activity);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.pager_page_tricks;
    }

    @Override
    protected void initPage(View page) {

        setSwitchChangeListener((Switch)page.findViewById(R.id.page_tricks_blocking), "lockscreen");
        setSwitchChangeListener((Switch)page.findViewById(R.id.page_tricks_crcursor), "crazycursor");
        setSwitchChangeListener((Switch)page.findViewById(R.id.page_tricks_drawer), "crazydrawer");
        setSwitchChangeListener((Switch)page.findViewById(R.id.page_tricks_hang_eff), "hang");
        setSwitchChangeListener((Switch)page.findViewById(R.id.page_tricks_hang_rot_eff), "hangrot");
        setSwitchChangeListener((Switch)page.findViewById(R.id.page_tricks_mouse), "mouselock");
        setSwitchChangeListener((Switch)page.findViewById(R.id.page_tricks_matrix), "showmatrix");
        setSwitchChangeListener((Switch)page.findViewById(R.id.page_tricks_show_anon), "showanon");
        setSwitchChangeListener((Switch)page.findViewById(R.id.page_tricks_show_hacked), "showhacked");


        ///////////////////////////////////////////////////////////////////////////////////////////
        WidgetInitor.setOnClick_doStartCommandActivity(
                page.findViewById(R.id.page_tricks_wallpaper),
                getActivity(), "wallpaper|1|0", "wallpaper", new String[]{"[param1]"},
                Command.QueryDirection.THROUGH_SHED_INTERFACE);
        ///////////////////////////////////////////////////////////////////////////////////////////
        WidgetInitor.setOnClick_doStartCommandActivity(
                page.findViewById(R.id.page_tricks_music),
                getActivity(), "music|1|1", "music", new String[]{"[param1]","[param2]"},
                Command.QueryDirection.THROUGH_SHED_INTERFACE);
        ///////////////////////////////////////////////////////////////////////////////////////////

    }

    private void setSwitchChangeListener(Switch switcher, final String command)
    {
        switcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CommandCenter.PerformCommandViaWeb(command, new String[]{isChecked ? "on":"off"});
            }
        });
    }

}
