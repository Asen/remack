package qmex.antochi.samsung.remack.Memo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.SharedPrefs;

public class MemoValueAdder extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo_adder);
        //setTitleColor(getResources().getColor(R.color.button_text_color));

        final EditText keyField = (EditText)findViewById(R.id.memo_routine_key);
        final EditText valueField = (EditText)findViewById(R.id.memo_routine_value);
        final Button addButton = (Button)findViewById(R.id.memo_routine_add_button);
        final Button cancelButton = (Button)findViewById(R.id.memo_routine_cancel_button);

        keyField.setText(getIntent().getStringExtra("value"));

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = keyField.getText().toString().trim();
                String value = valueField.getText().toString().trim();
                if(key.length()==0 || value.length()==0) return;
                SharedPrefs.addNote(key, value);
                Intent data = new Intent();
                data.putExtra("key", key);
                data.putExtra("value", value);
                setResult(RESULT_OK, data);
                finish();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

    }
}
