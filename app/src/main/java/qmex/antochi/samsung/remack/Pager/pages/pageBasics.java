package qmex.antochi.samsung.remack.Pager.pages;

import android.app.Activity;
import android.view.View;

import qmex.antochi.samsung.remack.Command;
import qmex.antochi.samsung.remack.Pager.AbstractPage;
import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.WidgetInitor.WidgetInitor;

public class pageBasics extends AbstractPage {

    public pageBasics(Activity activity) {
        super(activity);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.pager_page_basics;
    }

    @Override
    protected void initPage(View page) {

        WidgetInitor.setOnClick_doStartCommandActivity(page.findViewById(R.id.web),
                getActivity(), "web|1|1", "hsend", new String[]{"web","[param1]","[param2]"},
                Command.QueryDirection.THROUGH_SHED_INTERFACE);
        ///////////////////////////////////////////////////////////////////////////////////////////
        WidgetInitor.setOnClick_doStartCommandActivity(page.findViewById(R.id.webhidden),
                getActivity(), "webhidden|1|1", "hsend", new String[]{"webhidden","[param1]","[param2]"},
                Command.QueryDirection.THROUGH_SHED_INTERFACE);
        ///////////////////////////////////////////////////////////////////////////////////////////
        WidgetInitor.setOnClick_doStartCommandActivity(page.findViewById(R.id.hidetop),
                getActivity(), "hidetop|0|0", "hsend", new String[]{"hidetop"},
                Command.QueryDirection.THROUGH_SHED_INTERFACE);
        ///////////////////////////////////////////////////////////////////////////////////////////
        WidgetInitor.setOnClick_doStartCommandActivity(page.findViewById(R.id.closetop),
                getActivity(), "closetop|0|0", "hsend", new String[]{"closetop"},
                Command.QueryDirection.THROUGH_SHED_INTERFACE);
        ///////////////////////////////////////////////////////////////////////////////////////////
        WidgetInitor.setOnClick_doStartCommandActivity(page.findViewById(R.id.shutdown),
                getActivity(), "cmd|0|0", "hsend", new String[]{"cmd", "shutdown /t 0 /f /s"},
                Command.QueryDirection.THROUGH_SHED_INTERFACE);
        ///////////////////////////////////////////////////////////////////////////////////////////
        WidgetInitor.setOnClick_doStartCommandActivity(page.findViewById(R.id.msg_and_shutdown),
                getActivity(), "msg|1|0", "hsend", new String[]{"cmd", "shutdown /t 5 /f /s /c \"[param1]\""},
                Command.QueryDirection.THROUGH_SHED_INTERFACE);
        ///////////////////////////////////////////////////////////////////////////////////////////
        WidgetInitor.setOnClick_doStartCommandActivity(page.findViewById(R.id.basics_reboot),
                getActivity(), "msg|0|0", "hsend", new String[]{"cmd", "shutdown /t 0 /f /r"},
                Command.QueryDirection.THROUGH_SHED_INTERFACE);
        ///////////////////////////////////////////////////////////////////////////////////////////
        WidgetInitor.setOnClick_doStartCommandActivity(page.findViewById(R.id.basics_hibernate),
                getActivity(), "msg|0|0", "hsend", new String[]{"cmd", "shutdown /f /h"},
                Command.QueryDirection.THROUGH_SHED_INTERFACE);
        ///////////////////////////////////////////////////////////////////////////////////////////
        WidgetInitor.setOnClick_doStartCommandActivity(page.findViewById(R.id.kill_process),
                getActivity(), "killprocess|1|0", "killprocess", new String[]{"[param1]"},
                Command.QueryDirection.THROUGH_SHED_INTERFACE);
        ///////////////////////////////////////////////////////////////////////////////////////////


    }

}
