package qmex.antochi.samsung.remack.Pager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import qmex.antochi.samsung.remack.Utils.App;

public abstract class AbstractPage {

    /* Encapsulation */
    private Activity activity = null;

    /* Standard Procedures */
    protected abstract void initPage(View page);
    protected abstract int getLayoutResource();
    public void processActivityResult(int requestCode, int resultCode, Intent data) { return; }
    public AbstractPage(Activity activity){ this.activity = activity; }
    protected Activity getActivity(){ return activity; }

    /* Main Method */
    public View getView()
    {
        String service = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater inflater = (LayoutInflater) App.getInstance().getSystemService(service);
        View page = inflater.inflate(getLayoutResource(), null);
        initPage(page);
        return page;
    }

}
