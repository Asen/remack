package qmex.antochi.samsung.remack.Utils.Command;

public class CommandCenter {

    public static void PerformCommandViaWeb(String command, String[] parameter)
    {
        String URL = URLCompleter.CompleteWeb(command, parameter);
        new CommandEntity(URL, CommandEntity.SyncType.Async).perform();
    }

    public static String DownloadParamAnswer(String command, String[] params)
    {
        String URL = URLCompleter.CompleteWeb(command, params);
        return URL==null ? "" : new CommandEntity(URL, CommandEntity.SyncType.Sync).perform();
    }

}
