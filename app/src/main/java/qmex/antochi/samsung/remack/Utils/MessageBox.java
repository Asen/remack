package qmex.antochi.samsung.remack.Utils;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import qmex.antochi.samsung.remack.R;

public class MessageBox {

    public static void Show(Context context, String title, String text, String btnText, final Runnable run)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setMessage(Html.fromHtml(text));
        alert.setCancelable(true);
        alert.setNeutralButton(btnText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                run.run();
                dialog.cancel();
            }
        });
        alert.show();

    }

    public static void ShowInfoDialog(Context context, String title, String text, String btnText, final Runnable run)
    {
        final Dialog dialog = new Dialog(context, R.style.AppDialog);
        dialog.setContentView(R.layout.dialog_messagebox);
        dialog.setTitle("Info");
        dialog.setCancelable(false);

        TextView captionView = (TextView)dialog.findViewById(R.id.dialog_mbox_caption);
        TextView textView = (TextView)dialog.findViewById(R.id.dialog_mbox_text);
        TextView buttonView = (Button)dialog.findViewById(R.id.dialog_mbox_button);

        captionView.setText(title);
        buttonView.setText(btnText);

        textView.setText(Html.fromHtml(text));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setLinksClickable(true);
        buttonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(run!=null) run.run();
                dialog.cancel();
            }
        });


        dialog.show();


    }

}
