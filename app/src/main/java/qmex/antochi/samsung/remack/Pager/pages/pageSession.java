package qmex.antochi.samsung.remack.Pager.pages;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import qmex.antochi.samsung.remack.Command;
import qmex.antochi.samsung.remack.Pager.AbstractPage;
import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.App;
import qmex.antochi.samsung.remack.Utils.Command.CommandCenter;

public class pageSession extends AbstractPage {

    private enum EchoResult{ ECHO_OK, ECHO_UNREACHABLE };

    double passedTime = 0.0;
    ImageView result = null;
    Resources resources = App.getInstance().getResources();

    public pageSession(Activity act) {
        super(act);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.pager_page_session;
    }

    @Override
    protected void initPage(View page) {

        TextView sessNameLabel = (TextView)page.findViewById(R.id.page_session_name);
        Button changeSessName = (Button)page.findViewById(R.id.change_session_name);
        result = (ImageView)page.findViewById(R.id.page_session_image_result);
        Button makeBeepButton = (Button)page.findViewById(R.id.page_session_make_beep);


        sessNameLabel.setText(App.getInstance().getOriginalSessCompName());
        changeSessName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        makeBeepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommandCenter.PerformCommandViaWeb("beep", new String[]{"null"});
            }
        });

        sessNameLabel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                    v.setBackgroundColor(R.color.pager_caption);
                else if(event.getAction()==MotionEvent.ACTION_UP) {
                    v.setBackgroundColor(Color.TRANSPARENT);
                    v.setTag(Integer.decode(v.getTag().toString())+1);
                    if(v.getTag().toString().equals("5")){
                        v.setTag("0");
                        Command.StartActivity(getActivity(),
                                "secret|1|1", "secret",
                                new String[]{"[param1]", "[param2]"},
                                Command.QueryDirection.THROUGH_SHED_INTERFACE);
                    }
                }
                return true;
            }
        });

        page.findViewById(R.id.page_session_check_availability).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                passedTime = 0;
                result.setImageResource(R.drawable.loading);
                result.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rotation));
                CommandCenter.PerformCommandViaWeb("sys", new String[]{"check_avail"});

                final Handler handler = new Handler();
                final Runnable run = new Runnable() {
                    @Override
                    public void run() {

                        final Runnable runner = this;

                        new AsyncTask<Void, Void, String>() {
                            @Override
                            protected String doInBackground(Void... params) {
                                return CommandCenter.DownloadParamAnswer("sys", new String[]{"get_avail"});
                            }

                            @Override
                            protected void onPostExecute(String ack) {
                                passedTime += 1.0;
                                if (ack.equals("1"))
                                    displayEchoResult(EchoResult.ECHO_OK);
                                else{
                                    if(passedTime>=30.0)
                                        displayEchoResult(EchoResult.ECHO_UNREACHABLE);
                                    else
                                        handler.postDelayed(runner, 1000);
                                }
                            }
                        }.execute();

                    }
                };

                handler.postDelayed(run, 500);

            }
        });

    }

    private void displayEchoResult(EchoResult echo)
    {
        result.clearAnimation();
        int resImage = echo==EchoResult.ECHO_OK ? R.drawable.connection_ok : R.drawable.connection_err;
        String resText = resources.getString( echo==EchoResult.ECHO_OK ? R.string.page_session_check_result_ok :
                R.string.page_session_check_result_bad );

        result.clearAnimation();
        result.setImageResource(resImage);
        Toast.makeText(getActivity(), resText, Toast.LENGTH_LONG).show();

    }

}
