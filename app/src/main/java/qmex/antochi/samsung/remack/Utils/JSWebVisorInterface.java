package qmex.antochi.samsung.remack.Utils;

import android.app.Activity;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.Command.CommandCenter;


public class JSWebVisorInterface {

    private Activity callingActivity = null;
    private WebView webview;
    private float scale = 1.0f;

    public JSWebVisorInterface(Activity called, WebView web)
    {
        callingActivity = called;
        webview = web;
        webview.setWebViewClient(new WebViewClient(){
            @Override
            public void onScaleChanged(WebView view, float oldScale, float newScale) {
                super.onScaleChanged(view, oldScale, newScale);
                scale = newScale;
            }
        });
    }

    @JavascriptInterface
    public void mouseClick(int x, int y, String button, int is_double)
    {
        CommandCenter.PerformCommandViaWeb("click", new String[]{x + ";;" + y + ";;" + button + ";;" + is_double});
        Msg(App.getInstance().getBaseContext().getResources().getString(R.string.page_extras_click_completed));
    }

    @JavascriptInterface
    public void pushKeys(String text)
    {
        CommandCenter.PerformCommandViaWeb("click_pushkeys", new String[]{text});
        Msg(App.getInstance().getBaseContext().getResources().getString(R.string.page_extras_pushkeys_completed));
    }

    @JavascriptInterface
    public String getSessName()
    {
        return App.getInstance().getSessCompName();
    }

    @JavascriptInterface
    public double getScale()
    {
        return scale;
    }

    @JavascriptInterface
    public void Msg(String msg)
    {
        Toast.makeText(callingActivity, msg, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public String getLocalizedText(String text)
    {
        if(text.equalsIgnoreCase("click_appeal_push_text"))
            return App.StringResource(R.string.page_extras_appeal_push);
        if(text.equalsIgnoreCase("click_left_button_text"))
            return App.StringResource(R.string.page_extras_click_left);
        if(text.equalsIgnoreCase("click_right_button_text"))
            return App.StringResource(R.string.page_extras_click_right);
        if(text.equalsIgnoreCase("click_pushkeys_box_text"))
            return App.StringResource(R.string.page_extras_click_pkboxtext);
        if(text.equalsIgnoreCase("click_pushkeys_button_text"))
            return App.StringResource(R.string.page_extras_click_pkbuttontext);
        if(text.equalsIgnoreCase("click_pushkeys_clear_text"))
            return App.StringResource(R.string.page_extras_click_clear);
        if(text.equalsIgnoreCase("click_servicekeys_text"))
            return App.StringResource(R.string.page_extras_pushkeys_service);

        if(text.equalsIgnoreCase("drawing_clear_text"))
            return App.StringResource(R.string.page_extras_drawing_clear_text);
        if(text.equalsIgnoreCase("drawing_draw_text"))
            return App.StringResource(R.string.page_extras_drawing_draw_text);

        return "!localized";
    }

}
