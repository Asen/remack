package qmex.antochi.samsung.remack.Memo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.SharedPrefs;

public class Memo extends Activity {

    public final static String RESULT_EXTRA = "result";
    public final static String RESULT_KEY_EXTRA = "keyname";
    public final static int MEMO_REQUEST = 0xEEE;
    private final static int REQUEST_ADD = 0xADD;
    private final static int REQUEST_PICK = 0x8C8;

    List<String> noteKeys = null;
    ArrayAdapter<String> notesAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo);
        //setTitleColor(getResources().getColor(R.color.button_text_color));

        ListView listNotes = (ListView)findViewById(R.id.memo_notes_list);
        HashMap<String, String> madeNotes = SharedPrefs.getAllNotes();
        noteKeys = new ArrayList<String>(madeNotes.keySet());
        performAlphabeticalSort();
        notesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, noteKeys);
        listNotes.setAdapter(notesAdapter);

        listNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Memo.this, MemoValuePicker.class);
                intent.putExtra("key", noteKeys.get(position));
                startActivityForResult(intent, REQUEST_PICK);
            }
        });

        Button addValueButton = (Button)findViewById(R.id.memo_add_button);
        addValueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Memo.this, MemoValueAdder.class);
                startActivityForResult(intent, REQUEST_ADD);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // [Private Inside] Adding Action
        if(requestCode==REQUEST_ADD)
        {
            if(resultCode==RESULT_CANCELED) {
                setResult(RESULT_CANCELED);
                finish();
                return;
            }
            if(resultCode==RESULT_OK){
                noteKeys.add(0, data.getStringExtra("key"));
                performAlphabeticalSort();
                notesAdapter.notifyDataSetChanged();
            }
        }

        // [Public] Picking Action
        if(requestCode==REQUEST_PICK)
        {
            if(resultCode==RESULT_CANCELED)
            {
                HashMap<String, String> madeNotes = SharedPrefs.getAllNotes();
                noteKeys.clear();
                noteKeys.addAll(madeNotes.keySet());
                performAlphabeticalSort();
                notesAdapter.notifyDataSetChanged();
            }

            if(resultCode==RESULT_OK){
                Intent intent = new Intent(Memo.this, MemoValuePicker.class);
                if(!data.hasExtra(Memo.RESULT_EXTRA)){
                    setResult(RESULT_CANCELED);
                    finish();
                    return;
                }
                intent.putExtra(Memo.RESULT_EXTRA, data.getStringExtra(Memo.RESULT_EXTRA));
                intent.putExtra(Memo.RESULT_KEY_EXTRA, data.getStringExtra(Memo.RESULT_KEY_EXTRA));
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    private void performAlphabeticalSort()
    {
        Comparator<String> comp = new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareToIgnoreCase(rhs);
            }
        };
        Collections.sort(noteKeys, comp);
    }

}
