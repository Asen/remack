package qmex.antochi.samsung.remack.Pager;

import android.content.res.Resources;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.App;

public class ViewPagerAdapter extends PagerAdapter {

    List<View> pages = null;

    public ViewPagerAdapter(List<View> pages){
        this.pages = pages;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        Resources res = App.getInstance().getResources();

        switch (position)
        {
            case 0: return res.getString(R.string.pager_titles_0);
            case 1: return res.getString(R.string.pager_titles_1);
            case 2: return res.getString(R.string.pager_titles_2);
            case 3: return res.getString(R.string.pager_titles_3);
            case 4: return res.getString(R.string.pager_titles_4);
            case 5: return res.getString(R.string.pager_titles_5);
            case 6: return res.getString(R.string.pager_titles_6);
            default: return "(default)";
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = pages.get(position);
        container.addView(v, 0);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }

    @Override
    public int getCount(){
        return pages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object){
        return view.equals(object);
    }

}
