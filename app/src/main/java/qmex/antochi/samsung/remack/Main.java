package qmex.antochi.samsung.remack;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import qmex.antochi.samsung.remack.Pager.AbstractPage;
import qmex.antochi.samsung.remack.Pager.Pager;
import qmex.antochi.samsung.remack.Pager.ViewPagerAdapter;
import qmex.antochi.samsung.remack.Pager.pages.pageExtras;
import qmex.antochi.samsung.remack.Pager.pages.pageModules;
import qmex.antochi.samsung.remack.Utils.App;
import qmex.antochi.samsung.remack.Utils.MessageBox;

public class Main extends Activity {

    private static final int MENU_INSTRUCTION = 1;
    private static final int MENU_DEVELOPERS  = 2;
    private static final int MENU_QUIT        = 3;

    Map<Integer, AbstractPage> pages = new LinkedHashMap<Integer, AbstractPage>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_pager);
        setTitle(App.getInstance().getActivityTitle());

        ArrayList<View> pageViews = new ArrayList<View>();
        Pager.Init(this);
        ////////////////////////////////////////////////
        pages.put(Pager.PAGE_SESSION, Pager.getPage(Pager.PAGE_SESSION));
        pages.put(Pager.PAGE_BASICS, Pager.getPage(Pager.PAGE_BASICS));
        pages.put(Pager.PAGE_MESSAGES, Pager.getPage(Pager.PAGE_MESSAGES));
        pages.put(Pager.PAGE_CMD, Pager.getPage(Pager.PAGE_CMD));
        pages.put(Pager.PAGE_JOKES, Pager.getPage(Pager.PAGE_JOKES));
        pages.put(Pager.PAGE_EXTRAS, Pager.getPage(Pager.PAGE_EXTRAS));
        pages.put(Pager.PAGE_MODULES, Pager.getPage(Pager.PAGE_MODULES));

        for(AbstractPage page : pages.values())
            pageViews.add(page.getView());

        /////////////////////////////////////////////////
        ViewPager commandPager = (ViewPager) findViewById(R.id.command_pager);
        ViewPagerAdapter pageAdapter = new ViewPagerAdapter(pageViews);
        commandPager.setAdapter(pageAdapter);
        commandPager.setCurrentItem(1);
    }

    @Override
    protected void onResume() {
        if(App.getInstance()==null || App.getInstance().isSessNameLost()) {
            finish();
            return;
        }
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0, MENU_INSTRUCTION, Menu.NONE, getString(R.string.menu_instruction));
        menu.add(0, MENU_DEVELOPERS, Menu.NONE, getString(R.string.menu_developers));
        menu.add(0, MENU_QUIT, Menu.NONE, getString(R.string.menu_quit));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case MENU_INSTRUCTION:
                Uri uriAboutPage = Uri.parse(getString(R.string.app_about_help_page));
                Intent aboutPage = new Intent(Intent.ACTION_VIEW, uriAboutPage);
                startActivity(aboutPage);
                break;
            case MENU_DEVELOPERS:
                showDevInfo();
                break;
            case MENU_QUIT:
                finish();
                break;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode)
        {
            case pageModules.MEMO_STORAGE_REQUEST:
                pages.get(Pager.PAGE_MODULES).processActivityResult(requestCode, resultCode, data);
                break;
            case pageExtras.CAMERA_REQUEST :
                pages.get(Pager.PAGE_EXTRAS).processActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void showDevInfo()
    {
        MessageBox.ShowInfoDialog(this, getString(R.string.devs_caption), getString(R.string.devs_content), "Ok", null);
    }

}
