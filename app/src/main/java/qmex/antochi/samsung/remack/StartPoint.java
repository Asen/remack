package qmex.antochi.samsung.remack;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import qmex.antochi.samsung.remack.Utils.App;
import qmex.antochi.samsung.remack.Utils.SharedPrefs;

public class StartPoint extends Activity {

    private static final int MENU_HELP = Menu.FIRST;
    private static final int MENU_CLEAN = Menu.FIRST+1;
    private static final int MENU_QUIT = Menu.FIRST+2;

    private AutoCompleteTextView sessNameField = null;
    private ArrayAdapter<String> sessNames = null;
    private ImageView remackLogo = null;
    private Toast badtryToast = null;
    private Button clearBtn = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_point);

        remackLogo = (ImageView)findViewById(R.id.remack_main_logo);
        sessNameField = (AutoCompleteTextView)findViewById(R.id.computer_session_name);
        sessNameField.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP)
                    hideInput();
                return false;
            }
        });

        updateSessionNamesAdapter();
        sessNameField.setAdapter(sessNames);

        clearBtn = (Button)findViewById(R.id.clear_sess_name);
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessNameField.setText("");
            }
        });

        remackLogo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                    remackLogo.setImageResource(R.drawable.icon_active);
                else if(event.getAction()==MotionEvent.ACTION_UP){
                    runCommandCenter();
                }
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        remackLogo.setImageResource(R.drawable.icon);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        hideInput();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_HELP, Menu.NONE, R.string.starter_menu_help);
        menu.add(0, MENU_CLEAN, Menu.NONE, R.string.starter_menu_clean);
        menu.add(0, MENU_QUIT, Menu.NONE, R.string.starter_menu_quit);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case MENU_HELP:
                Uri uriAboutPage = Uri.parse(getString(R.string.app_about_help_page));
                Intent aboutPage = new Intent(Intent.ACTION_VIEW, uriAboutPage);
                startActivity(aboutPage);
                break;
            case MENU_CLEAN:
                SharedPrefs.CleanComputerNamesHistory();
                updateSessionNamesAdapter();
                Toast.makeText(this, R.string.starter_history_clean, Toast.LENGTH_SHORT).show();
                break;
            case MENU_QUIT:
                finish();
                break;
        }

        return true;
    }

    private void updateSessionNamesAdapter()
    {
        if(sessNames==null) {
            int itemType = android.R.layout.simple_dropdown_item_1line;
            List<String> initialCollection = SharedPrefs.getComputerNames();
            sessNames = new ArrayAdapter<String>(this, itemType, initialCollection);
        }
        else{
            sessNames.clear();
            sessNames.addAll(SharedPrefs.getComputerNames());
            sessNames.notifyDataSetChanged();
        }
    }

    private void runCommandCenter()
    {
        if(sessNameField ==null) return;
        String gottenCName = sessNameField.getText().toString().trim();
        if(gottenCName.length()==0) {
            if(badtryToast!=null) badtryToast.cancel();
            badtryToast = Toast.makeText(this, getResources().getText(R.string.starter_toast_badtry), Toast.LENGTH_SHORT );
            badtryToast.show();
            remackLogo.setImageResource(R.drawable.icon);
            return;
        }

        hideInput();
        App.getInstance().setSessCompName(gottenCName);
        SharedPrefs.saveComputerName(gottenCName);
        updateSessionNamesAdapter();
        startActivity(new Intent(StartPoint.this, Main.class));
    }

    private void hideInput()
    {
        InputMethodManager inputManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(sessNameField.getWindowToken(), 0);
    }

}