package qmex.antochi.samsung.remack.Pager;

import android.app.Activity;

import qmex.antochi.samsung.remack.Pager.pages.pageBasics;
import qmex.antochi.samsung.remack.Pager.pages.pageCmd;
import qmex.antochi.samsung.remack.Pager.pages.pageExtras;
import qmex.antochi.samsung.remack.Pager.pages.pageMessages;
import qmex.antochi.samsung.remack.Pager.pages.pageModules;
import qmex.antochi.samsung.remack.Pager.pages.pageSession;
import qmex.antochi.samsung.remack.Pager.pages.pageTricks;

public class Pager {

    public static final int PAGE_SESSION = 0x1;
    public static final int PAGE_BASICS = 0x2;
    public static final int PAGE_MESSAGES = 0x3;
    public static final int PAGE_CMD = 0x4;
    public static final int PAGE_JOKES = 0x5;
    public static final int PAGE_EXTRAS = 0x6;
    public static final int PAGE_MODULES = 0x7;

    private static Activity context = null;

    public static void Init(Activity context)
    {
        Pager.context = context;
    }

    public static AbstractPage getPage(int page_id)
    {
        switch(page_id)
        {
            case PAGE_SESSION :
                return new pageSession(context);
            case PAGE_BASICS:
                return new pageBasics(context);
            case PAGE_MESSAGES:
                return new pageMessages(context);
            case PAGE_CMD:
                return new pageCmd(context);
            case PAGE_JOKES:
                return new pageTricks(context);
            case PAGE_EXTRAS:
                return new pageExtras(context);
            case PAGE_MODULES:
                return new pageModules(context);
        }
        return null;
    }

}
