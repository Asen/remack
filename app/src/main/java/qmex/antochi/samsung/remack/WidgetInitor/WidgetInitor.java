package qmex.antochi.samsung.remack.WidgetInitor;

import android.app.Activity;
import android.view.View;

import qmex.antochi.samsung.remack.Command;

public class WidgetInitor {

    public static void setOnClick_doStartCommandActivity(
            View view,final Activity context,final String template,
            final String command,final String[] params,final Command.QueryDirection is_direct)
    {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Command.StartActivity(context, template, command, params, is_direct);
            }
        });
    }

}
