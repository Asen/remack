package qmex.antochi.samsung.remack;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.net.URLEncoder;

import qmex.antochi.samsung.remack.Utils.App;
import qmex.antochi.samsung.remack.Utils.Command.CommandCenter;
import qmex.antochi.samsung.remack.Utils.Command.URLCompleter;
import qmex.antochi.samsung.remack.Utils.JSWebVisorInterface;

public class WebVisor extends Activity {

    Handler handler = new Handler();
    String waitingURL;
    Boolean isPageAlreadyLoaded = false;

    public static void Start(Context context, String waiting_file_name)
    {
        context.startActivity(getBasicIntent(context, waiting_file_name));
    }

    public static void StartWithInnerLoading(Context context, String waiting_file_name)
    {
        Intent intent = getBasicIntent(context, waiting_file_name);
        intent.putExtra("inner", true);
        context.startActivity(intent);
    }

    private static Intent getBasicIntent(Context context, String waiting_file_name)
    {
        if(waiting_file_name.indexOf("/")>0 || waiting_file_name.indexOf("\\")>0) {
            String[] particles = waiting_file_name.replace('\\', '/').split("\\/");
            waiting_file_name = particles[particles.length - 1];
        }
        Intent intent = new Intent(context, WebVisor.class);
        intent.putExtra("waiting_file_name", waiting_file_name);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_visor);
        setTitle(App.getInstance().getActivityTitle());

        final WebView vision = (WebView)findViewById(R.id.webvisor_webview);
        final String fileName = getIntent().getStringExtra("waiting_file_name");
        final Boolean isInnerLoading = getIntent().getBooleanExtra("inner", false);
        String tokenString = App.getInstance().getSessCompName() + "#####" + fileName;
        tokenString = Base64.encodeToString(tokenString.getBytes(), Base64.DEFAULT);

        try {
            tokenString = URLEncoder.encode(tokenString, "UTF-8");
        }catch(Exception e){};

        isPageAlreadyLoaded = false;
        waitingURL = URLCompleter.CompleteFileStorage() + "?token=" + tokenString;

        Runnable run = new Runnable() {
            @Override
            public void run() {

                final Runnable runner = this;

                new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... params) {
                        if(isPageAlreadyLoaded()) return "";
                        String[] parameters = new String[]{"is_file_exists",fileName};
                        return CommandCenter.DownloadParamAnswer("sys", parameters);
                    }
                    @Override
                    protected void onPostExecute(String ack) {
                        if(isPageAlreadyLoaded()) return;
                        if(ack.equals("OK")) {
                            isPageAlreadyLoaded = true;
                            if(isInnerLoading) {
                                JSWebVisorInterface js =  new JSWebVisorInterface(WebVisor.this, vision);
                                vision.addJavascriptInterface(js, "android");
                                vision.loadUrl(waitingURL);
                            }
                            else {
                                WebVisor.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(waitingURL)));
                                WebVisor.this.finish();
                            }
                        }
                        else if(handler!=null)
                            handler.postDelayed(runner, 1000);
                    }
                }.execute();

            }
        };

        vision.getSettings().setAppCacheEnabled(false);
        vision.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        vision.getSettings().setJavaScriptEnabled(true);
        vision.getSettings().setLoadWithOverviewMode(true);
        vision.getSettings().setUseWideViewPort(true);
        vision.getSettings().setBuiltInZoomControls(true);
        vision.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        vision.loadUrl("file:///android_asset/loading.html");
        handler.postDelayed(run, 1000);

    }

    private Boolean isPageAlreadyLoaded()
    {
        return isPageAlreadyLoaded;
    }

    @Override
    public void onBackPressed() {
        handler = null;
        finish();
    }
}
