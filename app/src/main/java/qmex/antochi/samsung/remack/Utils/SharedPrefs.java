package qmex.antochi.samsung.remack.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SharedPrefs {

    ////////////////////////////////////////////////////////////////////////
    private static final String COMPUTER_NAME = "computer_name";
    public static final String USER_STORAGE_KEY = "MY_STORAGE";
    ////////////////////////////////////////////////////////////////////////

    private static SharedPreferences getNotesPreferences()
    {
        Context context = App.getInstance().getBaseContext();
        return context.getSharedPreferences("Notes", Context.MODE_PRIVATE);
    }

    public static void addNote(String key, String value)
    {
        if(key.trim().length()==0 || value.trim().length()==0) return;

        if(key.equalsIgnoreCase("storage")) key = SharedPrefs.USER_STORAGE_KEY;
        SharedPreferences.Editor editor = getNotesPreferences().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void removeNote(String key)
    {
        SharedPreferences prefs = getNotesPreferences();
        if(prefs.contains(key)) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove(key);
            editor.commit();
        }
    }

    public static HashMap<String, String> getAllNotes()
    {
        SharedPreferences prefs = getNotesPreferences();
        HashMap<String, String> map = new HashMap<String, String>();
        for(Map.Entry<String, ?> entry : prefs.getAll().entrySet())
        if(entry.getKey().trim().length()>0 && entry.getValue().toString().trim().length()>0)
            map.put(entry.getKey(), entry.getValue().toString());
        return map;
    }

    ////////////////////////////////////////////////////////////////////////////

    private static SharedPreferences getNamesPreferences()
    {
        Context context = App.getInstance().getBaseContext();
        return context.getSharedPreferences("Names", Context.MODE_PRIVATE);
    }

    public static void saveComputerName(String name)
    {
        SharedPreferences prefs = getNamesPreferences();
        SharedPreferences.Editor editor = prefs.edit();
        if(!prefs.getAll().containsValue(name.trim()))
            editor.putString(COMPUTER_NAME+"_"+System.currentTimeMillis(), name.trim().toLowerCase());
        editor.commit();
    }

    public static List<String> getComputerNames()
    {
        SharedPreferences prefs = getNamesPreferences();
        ArrayList<String> names = new ArrayList<String>();
        for(String key : prefs.getAll().keySet()) {
            String name = prefs.getString(key, "").trim();
            names.add(name);
        }
        return names;
    }

    public static void CleanComputerNamesHistory()
    {
        SharedPreferences prefs = getNamesPreferences();
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

}
