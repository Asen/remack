package qmex.antochi.samsung.remack.Memo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.SharedPrefs;

public class MemoValuePicker extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo_value_picker);
        //setTitleColor(getResources().getColor(R.color.button_text_color));

        if(!getIntent().hasExtra("key")){
            finish();
            return;
        }

        final EditText valueField = (EditText)findViewById(R.id.memo_picker_value);
        ImageButton insertButton = (ImageButton)findViewById(R.id.memo_picker_insert);
        ImageButton removeButton = (ImageButton)findViewById(R.id.memo_picker_remove);

        final String gottenKey = getIntent().getStringExtra("key").trim();
        final String value = SharedPrefs.getAllNotes().get(gottenKey);
        valueField.setText(value);

        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                data.putExtra(Memo.RESULT_EXTRA, valueField.getText().toString());
                data.putExtra(Memo.RESULT_KEY_EXTRA, gottenKey);
                setResult(RESULT_OK, data);
                finish();
            }
        });

       removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getTag()==null){
                    v.setTag(System.currentTimeMillis());
                    String msg = getResources().getString(R.string.memo_picker_action_for_removing);
                    Toast.makeText(MemoValuePicker.this, msg, Toast.LENGTH_LONG).show();
                } else{
                    double period = (System.currentTimeMillis() - (Long)v.getTag()) / 1000.0;
                    v.setTag(System.currentTimeMillis());
                    if(period<1.0) {
                        SharedPrefs.removeNote(gottenKey);
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                }
            }
        });

    }

}
