package qmex.antochi.samsung.remack.Utils;

import android.app.Application;
import android.os.Environment;

import java.io.File;

import qmex.antochi.samsung.remack.R;

public class App extends Application {

    private static App application = null;
    public static String SESSION_NAME_UNDEF = "<undefined>";
    private static String SESSION_COMPUTER_NAME = SESSION_NAME_UNDEF;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
    }

    public static App getInstance(){
        return application;
    }

    public static String StringResource(int id){
        return App.getInstance().getApplicationContext().getResources().getString(id);
    }

    public static String getActivityTitle(){
        StringBuilder titleSessName = new StringBuilder();
        for(int i=0; i<SESSION_COMPUTER_NAME.length(); i+=2)
            titleSessName.append("*"+SESSION_COMPUTER_NAME.charAt(i));
        return StringResource(R.string.app_name) + " : " + titleSessName;
    }

    public static String getSdCardStoragepath()
    {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "remack64");
        if(!file.exists()) file.mkdirs();
        file.setWritable(true, false);
        return file.getAbsolutePath();
    }

    public static void setSessCompName(String name){
        SESSION_COMPUTER_NAME = name.trim().toLowerCase();
    }

    public static String getOriginalSessCompName(){
        return SESSION_COMPUTER_NAME;
    }

    public static String getSessCompName()
    {
        StringBuilder hash = new StringBuilder();
        for(int i=0; i<SESSION_COMPUTER_NAME.length(); ++i)
            hash.append( ((int)SESSION_COMPUTER_NAME.charAt(i) ^ i)+5000 );
        return hash.toString();
    }

    public boolean isSessNameLost()
    {
        return SESSION_COMPUTER_NAME.trim().length()==0 || SESSION_COMPUTER_NAME.equalsIgnoreCase(SESSION_NAME_UNDEF);
    }

}
