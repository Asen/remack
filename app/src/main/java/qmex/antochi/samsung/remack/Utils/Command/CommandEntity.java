package qmex.antochi.samsung.remack.Utils.Command;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.App;

public class CommandEntity {

    public static enum SyncType{ Async, Sync };

    private String URL;
    private SyncType syncType;

    public CommandEntity(String url, SyncType sync)
    {
        URL = url;
        syncType = sync;
    }

    public String perform()
    {
        return syncType.equals(SyncType.Async) ? performQueryAsync(URL) : performQuery(URL);
    }

    private static String performQueryAsync(final String URL)
    {
        final Context appContext = App.getInstance().getApplicationContext();

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                return CommandEntity.performQuery(URL);
            }

            @Override
            protected void onPostExecute(String ack) {
                Resources res = App.getInstance().getResources();
                String MSG = res.getString(R.string.command_center_run_successful);
                if(ack==null || ack.equals("-1"))
                    MSG = res.getString(R.string.command_center_run_failed);
                Toast.makeText(appContext, MSG, Toast.LENGTH_SHORT).show();
            }
        }.execute();

        return "";
    }

    private static String performQuery(String query)
    {
        //Log.e("Remack64_Query", query);
        try
        {
            java.net.URL url = new URL(query);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            BufferedReader buffer = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String line = "";
            StringBuilder request = new StringBuilder();
            while((line = buffer.readLine()) != null) request.append(line);
            buffer.close();
            //Log.e("Remack64_QuerySent", request.toString());
            return request.toString();
        }catch(Exception e){
            //Log.e("Remack64_Logging", "CommandEntity , performQuery ::: " + e.getMessage());
            return "-1";
        }
    }


}
