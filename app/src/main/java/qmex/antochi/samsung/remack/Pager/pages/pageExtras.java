package qmex.antochi.samsung.remack.Pager.pages;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.media.MediaRecorder;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

import qmex.antochi.samsung.remack.Pager.AbstractPage;
import qmex.antochi.samsung.remack.R;
import qmex.antochi.samsung.remack.Utils.App;
import qmex.antochi.samsung.remack.Utils.Command.CommandCenter;
import qmex.antochi.samsung.remack.Utils.Command.FileUploader;
import qmex.antochi.samsung.remack.Utils.ImageCompressor;
import qmex.antochi.samsung.remack.WebVisor;

public class pageExtras extends AbstractPage {

    public static final int CAMERA_REQUEST = 0x30;

    public pageExtras(Activity activity) {
        super(activity);
    }

    @Override
    public void processActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == pageExtras.CAMERA_REQUEST && resultCode==Activity.RESULT_OK)
        {
            String path = new File(App.getInstance().getSdCardStoragepath(), "tmp.jpg").getAbsolutePath();
            ImageCompressor bmp = new ImageCompressor(path);
            File photo = new File(bmp.getComressFile());
            FileUploader.Upload(photo, "camera");
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.pager_page_extras;
    }

    @Override
    protected void initPage(View page) {

        String uName = App.getInstance().getSessCompName();

        initButton((Button)page.findViewById(R.id.page_extras_getinfo), "info", uName, false);
        initButton((Button)page.findViewById(R.id.page_extras_getscreen), "screen", uName, false);
        initButton((Button)page.findViewById(R.id.page_extras_getplist), "plist", uName, false);
        initButton((Button)page.findViewById(R.id.page_extras_click), "click", "-1;-1;left;0", true);
        initCameraButton((Button)page.findViewById(R.id.page_extras_camera));
        initAudioButton((Button)page.findViewById(R.id.page_extras_audio));

        initSoundBarManager((TextView) page.findViewById(R.id.page_extras_sound_label),
                (SeekBar) page.findViewById(R.id.page_extras_sound_management));

    }

    private void
    initButton(Button btn, final String name, final String param, final Boolean doInnerLoad)
    {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommandCenter.PerformCommandViaWeb(name, new String[]{param});
                if(doInnerLoad)
                    WebVisor.StartWithInnerLoading(getActivity(), name);
                else
                    WebVisor.Start(getActivity(), name);
            }
        });
    }

    private void
    initCameraButton(Button btn)
    {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photo = new File(App.getInstance().getSdCardStoragepath(), "tmp.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                getActivity().startActivityForResult(intent, CAMERA_REQUEST);
            }
        });
    }

    private void
    initAudioButton(Button btn)
    {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MediaRecorder mediaRecorder = new MediaRecorder();
                final File output = new File(App.getInstance().getSdCardStoragepath(), "aud.3gpp");
                final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Dialog);
                dialog.setCancelable(true);
                dialog.setTitle(R.string.page_extras_start_record);
                dialog.setContentView(R.layout.dialog_audio_recording);
                final ImageButton cancel = (ImageButton)dialog.findViewById(R.id.audio_recording_cancel);
                cancel.setEnabled(false);
                dialog.findViewById(R.id.audio_recording_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(v.getTag()!=null) {
                            mediaRecorder.stop();
                            dialog.cancel();
                            FileUploader.Upload(output, "audio_rec");
                            return;
                        }
                        v.setTag(new Object());
                        ((ImageButton)v).setImageResource(R.drawable.stop_rec);
                        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                        mediaRecorder.setOutputFile(output.getAbsolutePath());
                        try {
                            mediaRecorder.prepare();
                            mediaRecorder.start();
                            cancel.setEnabled(true);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mediaRecorder.stop();
                        dialog.cancel();
                    }
                });

                dialog.show();
            }
        });
    }

    private void
    initSoundBarManager(final TextView soundLabel, final SeekBar soundManager)
    {
        final String sm = App.getInstance().getResources().getString(R.string.page_extras_sound_manager);
        soundManager.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                soundLabel.setText(sm+" "+progress+"%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                soundLabel.setText(sm+" 50%");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                CommandCenter.PerformCommandViaWeb("sound", new String[]{seekBar.getProgress()+""});
            }

        });
    }

}
